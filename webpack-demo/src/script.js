import _ from 'lodash';

/* *\***!  сложение двух чисел *** тесты с jest успешны !***\* */
const adder = (...nums) => { return nums.reduce((total, num) => total + num, 0); };
console.table(adder(2,2));

function component() {
    const element = document.createElement('div');
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    return element;
}
document.body.appendChild(component());