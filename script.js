const res = (x) => { return ((x + x + x) / 3) }
console.log(res(2))

// ** callback test1
function question(job) {
    const jobDic = {
        developer: 'Что такое JS?',
        teacher: 'Какой предмет вы ведете?',
    };
    return function (name) {
        return `${name}, ${jobDic[job]}`;
    };
}
// ** call1
const developerQuestion1 = question('developer');
console.table(developerQuestion1('Denis'));
// ** call2
const developerQuestion2 = question('teacher');
console.table(developerQuestion2('Denis'));

// ** callback test2
/*# !** *** */

/* *\***!  сложение двух чисел *** тесты с jest успешны !***\* */
const adder = (...nums) => { return nums.reduce((total, num) => total + num, 0); };
console.table(adder(2,2));


















